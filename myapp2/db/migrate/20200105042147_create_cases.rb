class CreateCases < ActiveRecord::Migration[6.0]
  def change
    create_table :cases do |t|
      t.string :name
      t.string :blood_type
      t.string :case
      t.string :gender

      t.timestamps
    end
  end
end
