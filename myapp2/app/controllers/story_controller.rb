class StoryController < ApplicationController
  def index
     @stories = Story.all
  end

  def create
     @story = Story.new(params.require(:story).permit(:story))
     @story.save
     redirect_to @story
  end

  def show
     @story = Story.find(params[:id])
  end
  
  def edit
     @story = Story.find(params[:id])
  end
  
  def update 
     @story = Story.find(params[:id])
     if @story.update(params.require(:story).permit(:story))
        redirect_to @story
     else
        render 'edit'
     end
  end

  def destroy
     @story = Story.find(params[:id])
     @story.destroy
  end
end
